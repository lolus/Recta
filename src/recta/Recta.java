/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recta;
import java.util.Scanner;


/**
 *
 * @author Vicente
 */
public class Recta {
    int A;
    int B;
    int C;
  
    
    
   /**
    * Constructor por parámetros
    */
Recta(int x, int y,int z){
        this.A=x;
        this.B=y;
        this.C=z;
    }
    
    private void setA(int a){
        this.A=a;
    }
    private void setB(int b){
        this.B=b;
    }
    private void setC(int c){
        this.C=c;
    }
    private int getA(){
        return this.A;
    }
    private int getB(){
        return this.B;
    }
    private int getC(){
        return this.C;
    }
    void show(){
        System.out.println("("+getA()+")x +("+getB()+")y +("+getC()+")");
    }
    
    /**
     * Método que transforma la ecuación general de la recta
     * en la forma punto pendiente
     * Ejercicio 2. Elaborado por Jesiica López Luna
     */
    
    void puntoPendiente (Recta r){
        int x1=0;
        float y1= -r.getC()/r.getB();
        float x2= -r.getC()/r.getA();
        int y2=0;
        float m;
        m=(y2-y1)/(x2-x1);
                
        System.out.println("Ecuación punto pendiente");
        System.out.println("Puntos:");
        System.out.println("P1=("+x1+","+y1+")P2=("+x2+","+y2+")");    
        System.out.println("y-"+y1+" = ("+m+")*(x - "+x1+")");
        }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int x,y,z;
    Scanner valores=new Scanner(System.in);
    System.out.println("La transforación de la recta es:");
    System.out.println("Cuaes son los parámetros para X,Y y Z?");
        x=valores.nextInt();
        y=valores.nextInt();
        z=valores.nextInt();
         Recta r=new Recta(x,y,z); 
         System.out.println("Tu ecuación en su forma punto pendiente es:");
         r.show();
         r.puntoPendiente(r);
        
        
        
    }
    
}
